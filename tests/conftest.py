from typing import List, Dict
import pytest
from fastapi.testclient import TestClient
from faststan.stan import FastSTAN
from faststan.nats import FastNATS
from pydantic import BaseModel


@pytest.fixture
def dummy_model():
    class DummyModel(BaseModel):
        foo: str

    return DummyModel


@pytest.fixture
def dummy_error_cb():
    return lambda x: print(x)


def nats(dummy_model, dummy_error_cb):
    nats_app = FastNATS()
    yield nats_app


@pytest.fixture
def stan(dummy_model, dummy_error_cb):
    stan_app = FastSTAN()
    yield stan_app
