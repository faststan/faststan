import os

if os.environ.get("ENVIRONMENT", "dev") == "dev":
    os.environ["PYTHONASYNCIODEBUG"] = "1"

import asyncio
import datetime
import json
from loguru import logger
from faststan.nats import FastNATS


async def run():
    async with FastNATS() as client:
        reply_msg = await client.request_json(
            "predict", {"values": [[0, 0, 0, 0]], "timestamp": 1602661983}
        )

    print(f"Received a reply: {reply_msg}")


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run())
