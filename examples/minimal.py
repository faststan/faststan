import os

if os.environ.get("ENVIRONMENT", "dev") == "dev":
    os.environ["PYTHONASYNCIODEBUG"] = "1"

from pydantic import BaseModel
from loguru import logger


class NewEvent(BaseModel):
    name: str


class Greetings(BaseModel):
    message: str


def on_event(event: NewEvent) -> Greetings:
    logger.debug(f"Received new event: {event}.")
    return Greetings(message=f"Welcome to {event.name}!")
