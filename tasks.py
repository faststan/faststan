"""
Development tasks for FastSTAN project
"""
import contextlib
import os
from pathlib import Path
from shutil import rmtree

from invoke import task


PROJECT_DIR = Path(__file__).parent
SRC_DIR = PROJECT_DIR / "src"
TESTS_DIR = PROJECT_DIR / "tests"


SRC_FILES = " ".join([str(path) for path in SRC_DIR.glob("**/*.py")])
TEST_FILES = " ".join([str(path) for path in TESTS_DIR.glob("**/*.py")])


@task
def example(c, name="faststan", host="127.0.0.1", port="4222"):
    """Run an example."""
    os.environ["STAN_HOST"] = host
    os.environ["STAN_PORT"] = port
    os.environ["STAN_CLIENT"] = f"example-{name}"
    with c.cd(f"./docs/examples/{name}"):
        if name == "faststan":
            c.run("uvicorn app:app")
        else:
            c.run("python app.py")


@task
def docs(c, coverage=True):
    """Serve the documentation."""
    if coverage:
        if not Path("docs/coverage-report/index.html").is_file():
            print("Running tests to generate coverage report.")
            c.run("pytest")
    c.run("mkdocs serve")


@task
def lint(c):
    """Lint the code sources."""
    c.run(f"flake8 {SRC_FILES} {TEST_FILES} tasks.py")


@task
def check(c):
    """Check type using mypy."""
    c.run("mypy src/faststan")


@task
def format(c):
    """Format the code sources."""
    c.run(f"black {SRC_FILES} {TEST_FILES} tasks.py")


@task
def test(c):
    """Run the unit tests."""
    c.run("pytest")


@task
def build(c, package=True, docs=True, coverage=True):
    """Build the package and optionally documentation."""
    if docs:
        if coverage:
            if not Path("docs/coverage-report/index.html").is_file():
                print("Running tests to generate coverage report.")
                c.run("pytest")
        c.run(f"mkdocs build -d {PROJECT_DIR / 'dist' / 'docs'}")
    if package:
        c.run("poetry build")


@task
def clean(c):
    """Clean the build artefacts."""

    def try_rm(path: Path):
        try:
            rmtree(path)
        except NotADirectoryError:
            path.unlink(missing_ok=True)
        except FileNotFoundError:
            pass

    try_rm(PROJECT_DIR / "dist")
    try_rm(PROJECT_DIR / "cov.xml")
    try_rm(PROJECT_DIR / "htmlcov")
    try_rm(PROJECT_DIR / ".coverage")
    [rmtree(directory) for directory in Path("src").glob("**/*.egg-info")]
    [rmtree(directory) for directory in Path("src").glob("**/__pycache__")]
    [rmtree(directory) for directory in Path("tests").glob("**/__pycache__")]


@task
def nb_kernel(c, name="faststan", display_name="FastSTAN"):
    """Register a jupyter kernel for current python interpreter."""
    c.run("python -m pip install ipykernel")
    c.run(
        f'python -m ipykernel install --user --name="{name}" --display-name="{display_name}"'
    )


@task
def build_ci_image(
    c, repository="gcharbon", name="faststan", tag="ci",
):
    cmd = f"docker build \
            -t {repository}/{name}:{tag} \
            -f ci/Dockerfile \
            ."
    c.run(cmd)


@task
def push_ci_image(c, repository="gcharbon", name="faststan", tag="ci"):
    c.run(f"docker push {repository}/{name}:{tag}")
