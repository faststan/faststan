::: faststan.nats.FastNATS
    rendering:
      show_root_heading: true

::: faststan.stan.FastSTAN
    rendering:
      show_root_heading: true

::: faststan.service.ServiceMixin
    rendering:
      show_root_heading: true

::: faststan.subscriber.SubscriberMixin
    rendering:
      show_root_heading: true

::: faststan.publisher.PublisherMixin
    rendering:
      show_root_heading: true
