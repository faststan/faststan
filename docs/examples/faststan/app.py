"""Minimal example of a FastSTAN application."""
from typing import List
from pydantic import BaseModel
from faststan import FastSTAN


class Event(BaseModel):
    timestamp: int
    temperature: float
    humidity: float


app = FastSTAN()


def handle_error(error):
    print(f"ERROR: {error}")


@app.stan.subscribe("event", error_cb=handle_error)
def on_event(event: Event):
    """Process new event."""
    msg = f"INFO: New event on timestamp: {event.timestamp}. Temperature: {event.temperature}. Humidity: {event.humidity}"
    print(msg)


@app.post("/events")
async def publish_event(event: Event):
    """Publish event to NATS streaming."""
    await app.stan.publish_pydantic_model("events", event)


@app.stan.subscribe("features")
def on_features(features: List[float]):
    """Process new features."""
    msg = f"INFO: New features. Total: {sum(features)}"
    print(msg)


@app.post("/features")
async def publish_features(features: List[float]):
    """Publish features to NATS streaming"""
    await app.stan.publish_json("features", features)
