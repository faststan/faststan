"""Minimal example of a FastSTAN application."""
import asyncio
from random import random
from time import time
from pydantic import BaseModel
from faststan.publisher import STANPublisher


class Event(BaseModel):
    timestamp: int
    temperature: float
    humidity: float


app = STANPublisher()


async def main():

    while True:
        event = {
            "temperature": 20 * random(),
            "humidity": 100 * random(),
            "timestamp": int(time()),
        }
        await app.publish_json("events", event)
        await asyncio.sleep(1)


if __name__ == "__main__":
    app.run_forever()
